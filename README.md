BEM-blocks
-----------

Global JS container for BEM-blocks.



Create script for block
========================

For example, we have BEM-block static/blocks/menu-button.

Just create file static/blocks/menu-button/menu-button.js with content:


```javascript

/**
 * Base script
 */
blocks.MenuButton = function () {
    function init() {
        console.log('MenuButton');
    }

    return {
        init: init
    };
};

// Run block
blocks.MenuButton().init();
```



Overriding of blocks
=====================

We created block MenuButton in example above.
Just imaging, that we need override that extend that block with new functional.
Create new BEM-block at static/blocks/menu-button-extended. 
And put file static/blocks/menu-button-extended/menu-button-extended.js with content:

```javascript
blocks.MenuButtonExtended = function () {
    var self = blocks.MenuButton;

    self.init = function() {
        console.log('MenuButtonExtended');
    };

    return self;
};


// Run overrided block
blocks.MenuButtonProject().init();
```