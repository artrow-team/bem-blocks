/**
 * Global JS container for BEM-blocks
 */
var blocks = (function (scope) {
    if (undefined === scope.blocks) {
        scope.blocks = {};
    }

    return scope.blocks;
})(window);
